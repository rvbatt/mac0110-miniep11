## MAC0110 - MiniEP11
## Nome: Rodrigo Volpe Battistin
## NUSP: 1795464

using Test
function testa()
	@testset "Palíndromo" begin
		@test palindromo("") == true
		@test palindromo("ovo") == true
		@test palindromo("MiniEP11") == false
		@test palindromo("Socorram-me, subi no ônibus em Marrocos!") == true
		@test palindromo("A mãe te ama") == true
		@test palindromo("Passei em MAC0110!") == false
		@test palindromo("âé!!ùÖ,121,oŨ+-=Ëá") == true
	end
	@testset "Processa String" begin
		@test processaString("") == ""
		@test processaString("OvO") == "ovo"
		@test processaString("MiniEP11") == "miniep11"
		@test processaString("Socorram-me, subi no ônibus em Marrocos!") == "socorrammesubinoonibusemmarrocos"
		@test processaString("A mãe te ama") == "amaeteama"
		@test processaString("01áàÂãäéÈêẽëíìîĨïóòÔõöúùûũÜç56") == "01aaaaaeeeeeiiiiiooooouuuuuc56"
	end
end

function processaString(string) # Função auxiliar de palindromo
	string = lowercase(string)
	resposta = ""
	validos = "abcdefghijklmnopqrstuvwxyz0123456789"
	a = "áàâãä"
	e = "éèêẽë"
	i = "íìîĩï"
	o = "óòôõö"
	u = "úùûũü"
	for caracter in string
		if caracter ∈ validos
			resposta = resposta * caracter
		else
			if caracter ∈ a
				resposta = resposta * "a"
			end
			if caracter ∈ e
				resposta = resposta * "e"
			end
			if caracter ∈ i
				resposta = resposta * "i"
			end
			if caracter ∈ o
				resposta = resposta * "o"
			end
			if caracter ∈ u
				resposta = resposta * "u"
			end
			if caracter == 'ç'
				resposta = resposta * "c"
			end
		end
	end
	return resposta
end

function palindromo(string)
	string = processaString(string)
	pontEsquerda = firstindex(string)
	pontDireita = lastindex(string)
	while pontEsquerda < pontDireita
		if string[pontEsquerda] != string[pontDireita]
			return false
		else
			pontEsquerda = nextind(string, pontEsquerda)
			pontDireita = prevind(string, pontDireita)
		end
	end
	return true
end

#testa()
